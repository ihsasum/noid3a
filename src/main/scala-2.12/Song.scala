import com.mpatric.mp3agic.Mp3File
import scalafx.beans.property.StringProperty

// https://github.com/mpatric/mp3agic
class Song (mp3path_ : Mp3File) {
  val mp3file = mp3path_
  val id3Tag = if (mp3file.hasId3v2Tag) mp3file.getId3v2Tag() else mp3file.getId3v1Tag
  val track = new StringProperty(this, "track", id3Tag.getTrack)
  val title = new StringProperty(this, "title", id3Tag.getTitle)
  val artist = new StringProperty(this, "artist", id3Tag.getArtist)
  val album = new StringProperty(this, "album", id3Tag.getAlbum)
  val genre = new StringProperty(this, "genre", id3Tag.getGenreDescription)
  val length = new StringProperty(this, "length", formatTime(mp3file.getLengthInSeconds))
  val year = new StringProperty(this, "year", id3Tag.getYear)
  val path = mp3path_.getFilename

  private def formatTime(seconds: Long) = {
    val buffer = new StringBuffer
    buffer.append(seconds / 60).append(':')
    buffer.append("%02d".format(seconds % 60))
    buffer.toString
  }

}