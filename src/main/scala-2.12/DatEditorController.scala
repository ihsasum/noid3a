import java.io._
import javax.imageio.ImageIO

import com.mpatric.mp3agic._

import scalafx.Includes._
import scalafx.embed.swing.SwingFXUtils
import scalafx.event.ActionEvent
import scalafx.scene.control._
import scalafx.scene.image.{Image, ImageView}
import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.AnchorPane
import scalafx.scene.media.{Media, MediaPlayer}
import scalafx.stage.FileChooser
import scalafx.stage.FileChooser.ExtensionFilter
import scalafxml.core.macros.sfxml


@sfxml
class DatEditorController(var musicPlayer : MediaPlayer,
                          datMainContainer: AnchorPane,
                          datTrackList: TableView[Song],
                          track: TableColumn[Song, String],
                          title: TableColumn[Song, String],
                          artist: TableColumn[Song, String],
                          album: TableColumn[Song, String],
                          length: TableColumn[Song, String],
                          year: TableColumn[Song, String],
                          genre: TableColumn[Song, String],
                          datAlbumArt: ImageView,
                          datOpenFileIcon: ImageView,
                          datPlaySongIcon: ImageView,
                          datStopSongIcon: ImageView,
                          datSaveSongIcon: ImageView,
                          datOpenFileButton: Button,
                          datPlaySongButton: Button,
                          datStopSongButton: Button,
                          datSaveSongButton: Button,
                          datTitle: TextField,
                          datArtist: TextField,
                          datAlbum: TextField,
                          datYear: TextField,
                          datTrack: TextField,
                          datGenreDropdown: ComboBox[String]) {

  track.cellValueFactory = { _.value.track }
  title.cellValueFactory = { _.value.title }
  artist.cellValueFactory = { _.value.artist }
  album.cellValueFactory = { _.value.album }
  length.cellValueFactory = { _.value.length }
  year.cellValueFactory = { _.value.year }
  genre.cellValueFactory = { _.value.genre }

  var isPlaying = false

  ID3v1Genres.GENRES.foreach(genre => {
    datGenreDropdown.getItems.add(genre)
  })

  val fileChooser = new FileChooser {
    title = "can i haz mp3"
    extensionFilters ++= Seq(
      new ExtensionFilter("me need mp3", Seq("*.mp3")),
    )
  }

  val imageChooser = new FileChooser {
    title = "can i haz img"
    extensionFilters ++= Seq(
      new ExtensionFilter("me need png/jpg", Seq("*.png", "*.jpg"))
    )
  }

  datOpenFileButton.onAction = (event: ActionEvent) => {
    openSongChooser()
  }

  datPlaySongButton.onAction = (event: ActionEvent) => {
    playSong()
  }

  datStopSongButton.onAction = (event: ActionEvent) => {
    stopSong()
  }

  datSaveSongButton.onAction = (event: ActionEvent) => {
    setNewSongTags()
    saveTagsToFile()
  }

  datAlbumArt.onMouseClicked = (event: MouseEvent) => {
    openImageChooser()
  }

  datTrackList.selectionModel.apply.selectedItem.onChange {
    val selectedSong = datTrackList.selectionModel().selectedItem.value
    datTitle.setText(selectedSong.title.value)
    datArtist.setText(selectedSong.artist.value)
    datAlbum.setText(selectedSong.album.value)
    datYear.setText(selectedSong.year.value)
    datTrack.setText(selectedSong.track.value)

    datPlaySongButton.setVisible(true)
    datStopSongButton.setVisible(true)
    datPlaySongIcon.setVisible(true)
    datStopSongIcon.setVisible(true)

    val id3v2Tag = if (selectedSong.mp3file.hasId3v2Tag) selectedSong.mp3file.getId3v2Tag
    else  selectedSong.mp3file.setId3v2Tag(new ID3v24Tag())

    if(id3v2Tag.asInstanceOf[ID3v2].getAlbumImage() != null) {
      datAlbumArt.setImage(new Image(new ByteArrayInputStream(id3v2Tag.asInstanceOf[ID3v2].getAlbumImage())))
    } else {
      println("fuck shit")
    }
  }

  def setNewSongTags(): Unit = {
    val selectedSong = datTrackList.selectionModel().selectedItem
        selectedSong.value.title.value = datTitle.text.value
        selectedSong.value.artist.value = datArtist.text.value
        selectedSong.value.album.value = datAlbum.text.value
        selectedSong.value.year.value = datYear.text.value
        selectedSong.value.track.value = datTrack.text.value
        selectedSong.value.genre.value = if (datGenreDropdown.getSelectionModel.isEmpty) "" else datGenreDropdown.getSelectionModel.getSelectedItem
  }

  def saveTagsToFile(): Unit = {
    val selectedSong = datTrackList.selectionModel().selectedItem

    val id3v2Tag = if (selectedSong.value.mp3file.hasId3v2Tag) selectedSong.value.mp3file.getId3v2Tag
    else  selectedSong.value.mp3file.setId3v2Tag(new ID3v24Tag())

    id3v2Tag.asInstanceOf[ID3v2].setTitle(datTitle.text.value)
    id3v2Tag.asInstanceOf[ID3v2].setArtist(datArtist.text.value)
    id3v2Tag.asInstanceOf[ID3v2].setAlbum(datAlbum.text.value)
    id3v2Tag.asInstanceOf[ID3v2].setYear(datYear.text.value)
    id3v2Tag.asInstanceOf[ID3v2].setTrack(datTrack.text.value)
    if(datGenreDropdown.getSelectionModel.getSelectedItem != null) {
      id3v2Tag.asInstanceOf[ID3v2].setGenre( ID3v1Genres.GENRES.indexOf(datGenreDropdown.getSelectionModel.getSelectedItem.toString))
    }

    val image = SwingFXUtils.fromFXImage(datAlbumArt.getImage, null)
    val s = new ByteArrayOutputStream()
    ImageIO.write(image, "png", s)
    val res = s.toByteArray
    s.close()
//    val bytes = new Array[Byte](file.length.toInt)
    id3v2Tag.asInstanceOf[ID3v2].setAlbumImage(res, id3v2Tag.asInstanceOf[ID3v2].getAlbumImageMimeType)

    selectedSong.value.mp3file.save(selectedSong.value.path.replace("\\", "/"))
  }

  def openSongChooser(): AnyVal = {
    val selectedSongs = fileChooser.showOpenMultipleDialog(datTrackList.scene.window.get())
    selectedSongs.foreach(song => {
      if(song != null) {
        datTrackList.items.get.add(new Song(new Mp3File(song.getPath)))
      }
    })
  }

  def openImageChooser(): AnyVal = {
    val selectedImage = imageChooser.showOpenDialog(datTrackList.scene.window.get())
    if(selectedImage != null) {
      val image = selectedImage
      datAlbumArt.setImage(SwingFXUtils.toFXImage(ImageIO.read(image), null))
    }
  }

  def playSong(): Unit = {
    if(isPlaying) {
      musicPlayer.stop()
    }

    val selectedSong = datTrackList.selectionModel().selectedItem.value
    val songPath = new File(selectedSong.path).toURI().toASCIIString()
    val song = new Media(songPath)
    musicPlayer = new MediaPlayer(song)
    musicPlayer.play
    isPlaying = true
  }

  def stopSong(): Unit = {
    musicPlayer.stop
    isPlaying = false
  }

}