// https://github.com/mpatric/mp3agic
import java.io.IOException
import javafx.scene.image.Image

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafxml.core.{FXMLView, NoDependencyResolver}

object DatGui extends JFXApp {
  val resource = getClass.getResource("DatGui.fxml")
  if (resource == null) {
    throw new IOException("Cannot load resource: DatGui.fxml")
  }

  val root = FXMLView(resource, NoDependencyResolver)

  stage = new PrimaryStage() {
    title = "gui making be hard"
    scene = new Scene(root) {
      stylesheets = List(getClass.getResource("dateditor.css").toExternalForm)
    }
  }

  val paoFace = new Image(getClass.getResource("pao.jpg").toExternalForm)
  stage.setResizable(false)
  stage.setMaxHeight(675.0)
  stage.setMaxWidth(1400.0)
  stage.icons.add(paoFace)
}