name := "noid3a"

version := "1.0"

scalaVersion := "2.12.3"

libraryDependencies += "com.mpatric" % "mp3agic" % "0.8.1"
libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.102-R11"
libraryDependencies += "org.scalafx" %% "scalafxml-core-sfx8" % "0.3"

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)